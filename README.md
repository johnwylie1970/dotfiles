## Dotfiles - My config settings for Vim on MacOS

## Description

Backing up or keeing configuration for vim syncronised across multiple computers is a pain, so we use git to keep things in sync.

Mainly this is my vim config, but may expand to other thing at later date.

## Usage

* clone repo in ~/ home folder, it should be named ~/dotfiles
* cd dotfiles
* the config files are in this directory, so delete those in the home folder ans symlink to these insted.
```
 ln -s .vimrc ~/.vimrc
 ln -s .vim/ ~/.vim
```

Finally run the setup script
```
./setup.sh
```
