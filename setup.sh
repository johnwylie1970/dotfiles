#!/bin/bash
Echo 'Installing Vundle'
git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
brew install cmake
brew install go
brew install rust
mkdir ~/.vim/bundle/colors
cd ~/.vim/bundle/YouCompleteMe
./install.py --clang-completer --all
vim +PluginInstall +qall
touch ~/.tern-project
echo '{
  "plugins": {
    "node": {},
    "es_modules": {}
  },
  "libs": [
    "ecma5",
    "ecma6"
  ],
  "ecmaVersion": 6
}"' > ~/.tern-project 

