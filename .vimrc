set nocompatible              " be iMproved, required
filetype off                  " required
set shell=/bin/bash

" Fix files with prettier, and then ESLint.
let g:ale_fix_on_save = 1
let g:airline#extensions#ale#enabled = 1
let g:airline#extensions#tabline#enabled = 1
let g:airline_powerline_fonts = 1
let g:ale_set_balloon=1 
let g:ale_linters = {'javascript': ['prettier', 'eslint']}
"let g:ale_fixers = ['prettier', 'eslint']
"let g:ale_fixers = {
"\   '*': ['remove_trailing_lines', 'trim_whitespace'],
"\   'javascript': ['prettier', 'eslint'],
"\}
let g:ale_fixers = {}
let g:ale_fixers.javascript = [
\ 'eslint',
\ 'prettier',
\ 'remove_trailing_lines']

" set the runtime path to include Vundle and initialize
"set rtp+=~/.vim/bundle/Vundle.vim
call plug#begin('~/.vim/plugged')

" let Vundle manage Vundle, required
"Plug 'itchyny/lightline.vim'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'junegunn/fzf'
Plug 'junegunn/fzf.vim'
Plug 'mattn/emmet-vim'
Plug 'scrooloose/nerdtree'
Plug 'Xuyuanp/nerdtree-git-plugin'
Plug 'terryma/vim-multiple-cursors'
Plug 'tpope/vim-eunuch'
Plug 'tpope/vim-surround'
Plug 'w0rp/ale'
Plug 'sbdchd/neoformat'
Plug 'kchmck/vim-coffee-script'
Plug 'editorconfig/editorconfig-vim'
Plug 'ervandew/supertab'
Plug 'Valloric/YouCompleteMe', { 'do': './install.py' }
Plug 'SirVer/ultisnips'
Plug 'honza/vim-snippets'
Plug 'janko-m/vim-test'
Plug 'YorickPeterse/happy_hacking.vim'
Plug 'gabrielelana/vim-markdown'
Plug 'JamshedVesuna/vim-markdown-preview'
Plug 'flowtype/vim-flow'
Plug 'sheerun/vim-polyglot'
Plug 'terryma/vim-smooth-scroll'

" make YCM compatible with UltiSnips (using supertab)
let g:ycm_key_list_select_completion = ['<C-n>', '<Down>']
let g:ycm_key_list_previous_completion = ['<C-p>', '<Up>']
let g:SuperTabDefaultCompletionType = '<C-n>'
let vim_markdown_preview_github=1

" better key bindings for UltiSnipsExpandTrigger
let g:UltiSnipsExpandTrigger = "<tab>"
let g:UltiSnipsJumpForwardTrigger = "<tab>"
let g:UltiSnipsJumpBackwardTrigger = "<s-tab>"
" All of your Plugs must be added before the following line
call plug#end()            " required
syntax enable
colorscheme happy_hacking
set number
filetype plugin indent on    " required
map ; :Files<CR>

" neoformat options
autocmd BufWritePre *.js Neoformat

" vim-jest
" these "Ctrl mappings" work well when Caps Lock is mapped to Ctrl
nmap <silent> t<C-n> :TestNearest<CR>
nmap <silent> t<C-f> :TestFile<CR>
nmap <silent> t<C-s> :TestSuite<CR>
nmap <silent> t<C-l> :TestLast<CR>
nmap <silent> t<C-g> :TestVisit<CR>

" smooth-scroll
noremap <silent> <c-u> :call smooth_scroll#up(&scroll, 50, 2)<CR>
noremap <silent> <c-d> :call smooth_scroll#down(&scroll, 50, 2)<CR>
noremap <silent> <c-b> :call smooth_scroll#up(&scroll*2, 50, 4)<CR>
noremap <silent> <c-f> :call smooth_scroll#down(&scroll*2, 50, 4)<CR>

""" Tabs #tabs
" - Two spaces wide
set tabstop=2
set softtabstop=2
" - Expand them all
set expandtab
" - Indent by 2 spaces by default
set shiftwidth=2
set mouse=a
set laststatus=2
set noshowmode
set nocompatible
set backspace=indent,eol,start
map <C-o> :NERDTreeToggle<CR>
let NERDTreeMinimalUI = 1
let NERDTreeDirArrows = 1
"autocmd VimEnter * NERDTree
"autocmd VimEnter * wincmd p
"
" Brief help
" :PlugList       - lists configured plugins
" :PlugInstall    - installs plugins; append `!` to update or just :PlugUpdate
" :PlugSearch foo - searches for foo; append `!` to refresh local cache
" :PlugClean      - confirms removal of unused plugins; append `!` to auto-approve removal
"
" see :h vundle for more details or wiki for FAQ
" Put your non-Plug stuff after this line
